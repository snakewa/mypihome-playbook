#!/usr/bin/env python

import time
import signal
import subprocess


import scrollphat

print("""
Scroll pHAT HD: Hello World

Scrolls "Hello World" across the screen
in a 5x7 pixel large font.

Press Ctrl+C to exit!

""")

# Uncomment to rotate the text
#scrollphat.rotate(180)

# Set a more eye-friendly default brightness
scrollphat.set_brightness(1)

msg = "Hello World! "
scrollphat.write_string(msg)

while True:
    hostname = subprocess.check_output(['/bin/hostname', '-I'])
    if msg != hostname:
        msg = hostname
        scrollphat.clear()
        scrollphat.write_string(" *** IP: " + msg.strip())
    scrollphat.scroll()
    time.sleep(0.1)